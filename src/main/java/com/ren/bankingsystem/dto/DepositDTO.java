package com.ren.bankingsystem.dto;

import com.ren.bankingsystem.enums.DepositType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by My PC on 11/4/2017.
 */
@Component
public class DepositDTO {
    private String depositNumber;

    private BigDecimal balance;
    private DepositType depositType;
    private ArrayList<TransactionDTO> transactions;
    public DepositDTO() {
    }

    public DepositDTO(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public DepositDTO(String depositNumber, BigDecimal balance, DepositType depositType) {
        this.depositNumber = depositNumber;
        this.balance = balance;
        this.depositType = depositType;
    }

    public DepositDTO(String depositNumber, ArrayList<TransactionDTO> transactions, DepositType depositType, BigDecimal balance) {

        this.depositNumber = depositNumber;
        this.transactions = transactions;
        this.depositType = depositType;
        this.balance = balance;
    }

    public DepositDTO(String depositNumber, DepositType depositType) {

        this.depositNumber = depositNumber;
        this.depositType = depositType;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public ArrayList<TransactionDTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<TransactionDTO> transactions) {
        this.transactions = transactions;
    }
}