package com.ren.bankingsystem.dto;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by My PC on 11/4/2017.
 */
@Component
public class CustomerDTO {
    private String name;
    private String familyName;
    private String customerNumber;
    private String email;
    private List<DepositDTO> deposits;


    public CustomerDTO(){
    }

    public CustomerDTO(String name, String familyname,String customerNumber ,String email){
        this.name=name;
        this.familyName=familyname;
        this.customerNumber=customerNumber;
        this.email=email;
    }

    public CustomerDTO(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setName(String name){
        this.name=name;
    }
    public void setFamilyName(String familyName){
        this.familyName=familyName;
    }
    public void setCustomerNumber(String customerNumber)
    {
        this.customerNumber=customerNumber;
    }
    public void setEmail(String email){
        this.email=email;
    }

    public String getName(){
        return name;
    }
    public String getFamilyName(){
        return familyName;
    }
    public String getCustomerNumber(){
        return customerNumber;
    }
    public String getEmail(){
        return email;
    }

    public List<DepositDTO> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<DepositDTO> deposits) {
        this.deposits = deposits;
    }

}
