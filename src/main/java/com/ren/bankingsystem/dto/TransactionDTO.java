package com.ren.bankingsystem.dto;

import com.ren.bankingsystem.enums.TransactionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by My PC on 12/15/2017.
 */
public class TransactionDTO {

    private String depositNumber;
    private BigDecimal amount;
    private TransactionType transactionType;
    private Date lastDepositOfProfit;

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositId) {
        this.depositNumber = depositId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getLastDepositOfProfit() {
        return lastDepositOfProfit;
    }

    public void setLastDepositOfProfit(Date lastDepositOfProfit) {
        this.lastDepositOfProfit = lastDepositOfProfit;
    }

    public TransactionDTO(String depositId, BigDecimal amount, TransactionType transactionType, Date lastDepositOfProfit) {

        this.depositNumber = depositId;
        this.amount = amount;
        this.transactionType = transactionType;
        this.lastDepositOfProfit = lastDepositOfProfit;
    }
}
