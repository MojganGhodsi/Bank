package com.ren.bankingsystem.dto;


import com.ren.bankingsystem.enums.ResponseStatus;

/**
 * Created by My PC on 12/16/2017.
 */
public class ResponseDTO<T> {

    private ResponseStatus responseStatus;
    private T response;
    private String msg;

    public ResponseDTO(ResponseStatus responseStatus, T response, String msg) {
        this.responseStatus = responseStatus;
        this.response = response;
        this.msg = msg;
    }

    public ResponseDTO() {
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
