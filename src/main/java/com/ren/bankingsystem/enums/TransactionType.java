package com.ren.bankingsystem.enums;

/**
 * Created by My PC on 12/11/2017.
 */
public enum TransactionType {

    DEPOSIT , WITHDRAW , TRANSFER , PROFIT
}
