package com.ren.bankingsystem.exception;

public class UsedCustomerIdException extends GeneralException{

    public UsedCustomerIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsedCustomerIdException(Throwable cause) {
        super(cause);
    }

    public UsedCustomerIdException(String message) {
        super(message);
    }
}
