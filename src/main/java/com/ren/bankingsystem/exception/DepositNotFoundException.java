package com.ren.bankingsystem.exception;

public class DepositNotFoundException extends GeneralException {

    public DepositNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DepositNotFoundException(Throwable cause) {
        super(cause);
    }

    public DepositNotFoundException(String message) {
        super(message);
    }
}
