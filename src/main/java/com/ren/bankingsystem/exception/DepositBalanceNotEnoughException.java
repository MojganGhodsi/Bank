package com.ren.bankingsystem.exception;

/**
 * Created by My PC on 12/16/2017.
 */

public class DepositBalanceNotEnoughException extends GeneralException {

    public DepositBalanceNotEnoughException(String message, Throwable cause) {
        super(message, cause);
    }

    public DepositBalanceNotEnoughException(Throwable cause) {
        super(cause);
    }

    public DepositBalanceNotEnoughException(String message) {
        super(message);
    }
}
