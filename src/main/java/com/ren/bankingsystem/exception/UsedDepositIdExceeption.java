package com.ren.bankingsystem.exception;

public class UsedDepositIdExceeption extends GeneralException {

    public UsedDepositIdExceeption(Throwable cause) {
        super(cause);
    }

    public UsedDepositIdExceeption(String message) {
        super(message);
    }

    public UsedDepositIdExceeption(String message, Throwable cause) {
        super(message, cause);
    }


}
