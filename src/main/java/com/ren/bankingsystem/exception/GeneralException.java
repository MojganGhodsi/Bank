package com.ren.bankingsystem.exception;

/**
 * Created by My PC on 12/10/2017.
 */
public class GeneralException extends Exception {

    public GeneralException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralException(Throwable cause) {

    }

    public GeneralException(String message) {
        super(message);
    }
}
