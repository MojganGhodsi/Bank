package com.ren.bankingsystem.exception;

public class CustomerNotFoundException extends GeneralException {

    public CustomerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerNotFoundException(Throwable cause) {
        super(cause);
    }

    public CustomerNotFoundException(String message) {
        super(message);
    }
}
