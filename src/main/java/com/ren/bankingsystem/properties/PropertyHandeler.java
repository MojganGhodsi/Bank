//package com.ren.bankingsystem.properties;
//
//import com.ren.bankingsystem.exception.GeneralException;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Properties;
//@Component
//public class PropertyHandeler {
//
//    private Properties properties = new Properties();
//
//    @PostConstruct
//    public void loadProperties() throws GeneralException {
//
//
//        String path = "config.properties";
//        ClassLoader classLoader = getClass().getClassLoader();
//
//        try {
//            classLoader.getResourceAsStream(path);
//            InputStream inputStream = classLoader.getResourceAsStream(path);
//            properties.load(inputStream);
//            inputStream.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//      /*try {
//
//
//         FileReader file = new FileReader("");
//         properties.load(file);
//
//      } catch (IOException e) {
//         throw new GeneralException("Problems found!" , e);
//      }*/
//
//    }
//
//    //call this method just for the first time that you run this app
//    public void setProperties() throws IOException, GeneralException {
//
//        try {
//            OutputStream os = new FileOutputStream("config.properties");
//
//            properties.put("LogFileDirectory"     , "d:\\CS-LogFile.txt");
////            properties.put("ReportImageDirectory" , "headerIcon.png");
////            properties.put("JsonFileDirectory"    , "d:\\CS-Customers.json");
////
////            properties.put("CustomersPdfFileDirectory"          , "\\CS-Customers.pdf");
////            properties.put("CustomersExcelFileDirectory"        , "\\CS-Customers.xlsx");
////            properties.put("DepositsPdfFileDirectory"           , "\\CS-Deposits.pdf");
////            properties.put("DepositsExcelFileDirectory"         , "\\CS-Deposits.xlsx");
////            properties.put("CustomerDepositsPdfFileDirectory"   , "\\CS-CustomerDeposits.pdf");
////            properties.put("CustomerDepositsExcelFileDirectory" , "\\CS-CustomerDeposits.xlsx");
//
//            properties.store(os , null);
//            os.close();
//        }
//        catch (Exception e){
//            throw new GeneralException("Problems found!" , e);
//        }
//
//    }
//
//    //to get the properties data
//    public String getProperties(String key){
//
//        return (String) properties.get(key);
//    }
//
//    }
//
//
