package com.ren.bankingsystem.control;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.entity.CustomerEntity;

/**
 * Created by My PC on 12/15/2017.
 */
public interface ICustomerService extends IService<CustomerEntity> {

    CustomerEntity insert(CustomerEntity costumer) throws GeneralException;

    CustomerEntity search(String param) throws GeneralException;

    CustomerEntity updateContactId(String formerId , String newId) throws GeneralException;

    byte[] generateBackup() throws GeneralException;

    byte[] returnBackup(byte[] content) throws GeneralException;

    void checkEmailValidation(String email) throws GeneralException;

    void shutdown();
}