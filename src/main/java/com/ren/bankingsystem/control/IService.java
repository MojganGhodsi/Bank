package com.ren.bankingsystem.control;

import com.ren.bankingsystem.exception.GeneralException;
import net.sf.dynamicreports.report.exception.DRException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by My PC on 12/15/2017.
 */
public interface IService<T> {

    void delete(T t) throws GeneralException;

    byte[] createReport(String type) throws GeneralException, IOException, DRException;

    ArrayList<T> getAllInf() throws GeneralException;

    void checkIdValidation(T t) throws GeneralException;
}
