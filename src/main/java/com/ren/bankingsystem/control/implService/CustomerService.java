package com.ren.bankingsystem.control.implService;

import com.ren.bankingsystem.control.ICustomerService;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.dao.ICustomerDAO;
import com.ren.bankingsystem.model.entity.CustomerEntity;
import net.sf.dynamicreports.report.exception.DRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by My PC on 11/4/2017.
 */
@Component("CustomerService")
public class CustomerService implements ICustomerService {

    private ICustomerDAO customerDAO;
    @Autowired
    public CustomerService(ICustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Override
    public CustomerEntity insert(CustomerEntity customer) throws GeneralException {

        try {

            checkIdValidation(customer);
            CustomerEntity customerEntity = customerDAO.insert(customer);
            return customerEntity;

        }catch (GeneralException e){
            //logHandler.setWarningLog(e.toString());
            throw e;
        }
    }

    @Override
    public CustomerEntity updateContactId(String formerId, String newId) throws GeneralException {

        CustomerEntity customer = customerDAO.search(formerId);
        customer.setCustomerNumber(newId);
        customerDAO.update(customer);
        return customer;
    }

    @Override
    public void checkIdValidation(CustomerEntity customerEntity) throws GeneralException {

        String id = customerEntity.getCustomerNumber();
        if (!Objects.equals(id, null)) {
            String regex = "\\d{8}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(id);
            if (!matcher.matches()) {
                throw new GeneralException("Invalid customer number! The id should contain 8 digits");
            }
        }
        else {
            throw new GeneralException("Don't leave it empty!");
        }

        if(!customerDAO.checkIdValidation(id)){
            throw new GeneralException("Invalid customer number! this customer number is been used!");
        }
    }

    @Override
    public void checkEmailValidation(String email) throws GeneralException {

        if (null != email) {
            String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) {
                throw new GeneralException("Invalid Email!");
            }
        }
        else{
            throw new GeneralException("You must enter this item!");
        }

    }

    @Override
    public void delete(CustomerEntity customerEntity) throws GeneralException {

        CustomerEntity customer = customerDAO.search(customerEntity.getCustomerNumber());
        customerDAO.delete(customer);

    }

    @Override
    public byte[] generateBackup() throws GeneralException {
        return new byte[0];
    }

    @Override
    public byte[] returnBackup(byte[] content) throws GeneralException {
        return new byte[0];
    }

    @Override
    public CustomerEntity search(String param) throws GeneralException {
        return null;
    }

    @Override
    public byte[] createReport(String type) throws GeneralException, IOException, DRException {
        return new byte[0];
    }

    @Override
    public void shutdown() {

    }

    @Override
    public ArrayList<CustomerEntity> getAllInf() throws GeneralException {
        return null;
    }

//    @Override
//    public void insertCustomer(CustomerEntity customerEntity) {
//
//        try {
//            customerDAO.insert(customerEntity);
//        } catch (GeneralException e) {
//            e.printStackTrace();
//        }
//
//    }
}
