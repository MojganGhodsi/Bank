package com.ren.bankingsystem.control.implService;

import com.ren.bankingsystem.control.IDepositService;
import com.ren.bankingsystem.enums.TransactionType;
import com.ren.bankingsystem.exception.DepositBalanceNotEnoughException;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.dao.ICustomerDAO;
import com.ren.bankingsystem.model.dao.IDepositDAO;
import com.ren.bankingsystem.model.entity.CustomerEntity;
import com.ren.bankingsystem.model.entity.DepositEntity;
import com.ren.bankingsystem.model.entity.TransactionEntity;
import net.sf.dynamicreports.report.exception.DRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by My PC on 12/15/2017.
 */
@Component("DepositService")
public class DepositService implements IDepositService {

    private IDepositDAO depositDAO;
    private ICustomerDAO customerDAO;
//    private LogHandler logHandler;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    public DepositService(IDepositDAO depositDAO, ICustomerDAO customerDAO) {
        this.depositDAO = depositDAO;
        this.customerDAO= customerDAO;
  //      this.logHandler = logHandler;
    }

    private Date getCurrentDate() {
        return new Date();
    }

    private ConcurrentMap<Integer, Integer> locks = new ConcurrentHashMap<Integer, Integer>();

    private Object getCacheSyncObject(final Integer id) {
        locks.putIfAbsent(id, id);
        return locks.get(id);
    }

    @Override
    public DepositEntity insert(DepositEntity deposit, String costumerId) throws GeneralException {

        try{

            checkIdValidation(deposit);
//            deposit.setLastDepositOfProfit(getCurrentDate());
            CustomerEntity costumer = customerDAO.search(costumerId);
            if(!Objects.equals(costumer.getDeposits() , null)){

                costumer.getDeposits().add(deposit);
            }
            else{
                ArrayList<DepositEntity> newDeposits = new ArrayList<>();
                newDeposits.add(deposit);
                costumer.setDeposits(newDeposits);
            }
            customerDAO.update(costumer);
            return deposit;
        }catch (NoResultException e){
           // logHandler.setWarningLog(e.toString());
            throw new GeneralException("The customer not found" , e);
        } catch (Exception e){
        //    logHandler.setWarningLog(e.toString());
            throw new GeneralException(e.getMessage() , e);
        }
    }

    @Override
    public void delete(DepositEntity depositEntity)throws GeneralException {
        {

            try {
                DepositEntity de = depositDAO.search(depositEntity.getDepositNumber());
                depositDAO.delete(de);

            }catch (PersistenceException e){
               // logHandler.setWarningLog(e.toString());
                throw new GeneralException("No deposit matches!", e);
            }
        }
    }

    @Override
    public DepositEntity updateDepositId(String costumerId, String formerDepositId, String newDepositId) throws GeneralException {
        try{
            customerDAO.checkIdValidation(costumerId);
            checkIdValidation(new DepositEntity(formerDepositId));
            checkIdValidation(new DepositEntity(newDepositId));
            CustomerEntity customer = customerDAO.search(costumerId);
            DepositEntity depositEntity = null;
            for (DepositEntity deposit : customer.getDeposits()) {

                if (Objects.equals(deposit.getDepositNumber() , formerDepositId)){
                    deposit.setDepositNumber(newDepositId);
                    depositEntity = deposit;
                    customerDAO.update(customer);
                    break;
                }else{
                    throw new GeneralException("No deposit matches!");
                }
            }

            return depositEntity;

        }catch (NoResultException e){

  //          logHandler.setWarningLog(e.toString());
            throw new GeneralException("No deposit matches!" , e);

        }catch (PersistenceException e){

 //           logHandler.setWarningLog(e.toString());
            throw new GeneralException("the deposit id is been used!" , e);
        }catch (GeneralException e){
            throw e;
        }
    }

    @Override
    public byte[] createDepositReportsForACostumer(String customerId, String type) throws GeneralException, IOException, DRException {
        return new byte[0];
    }

    @Override
    public BigDecimal deposit(String depositId, BigDecimal amount) throws GeneralException {
        {
            try {
                BigDecimal balance;
                checkIdValidation(new DepositEntity(depositId));
                DepositEntity deposit = depositDAO.search(depositId);

                synchronized (getCacheSyncObject(deposit.getId())){

                    depositDAO.refresh(deposit);
                    balance = deposit.getAmount();
                    balance = balance.add(amount);
                    deposit.setAmount(balance);
                    depositDAO.update(addTransaction(deposit , new TransactionEntity(amount, balance , TransactionType.DEPOSIT , getCurrentDate())));
                }

                return balance;

            }catch (Exception e){
//                logHandler.setWarningLog(e.toString());
                throw e;
            }
        }    }

    @Override
    public BigDecimal withdraw(String depositId, BigDecimal amount) throws GeneralException {
        try {
            BigDecimal balance = null;
            checkIdValidation(new DepositEntity(depositId));
            DepositEntity deposit = depositDAO.search(depositId);

            synchronized (getCacheSyncObject(deposit.getId())) {
                depositDAO.refresh(deposit);
                if (deposit.getAmount().compareTo(amount) >= 0) {

                    balance = deposit.getAmount();
                    balance = balance.subtract(amount);
                    deposit.setAmount(balance);
                    depositDAO.update(addTransaction(deposit, new TransactionEntity(amount.negate(), balance, TransactionType.WITHDRAW, getCurrentDate())));
                }else {
                    throw new DepositBalanceNotEnoughException("Balance is not enough!");
                }
            }
            return balance;

        }catch (Exception e){
//            logHandler.setWarningLog(e.toString());
            throw e;
        }    }

    @Override
    public void transfer(String originDepositId, String destinationDepositId, BigDecimal amount) throws GeneralException {
        {

            BigDecimal originBalance;
            BigDecimal destinationBalance;

            try {
                checkIdValidation(new DepositEntity(originDepositId));
                checkIdValidation(new DepositEntity(destinationDepositId));
                DepositEntity originDeposit = depositDAO.search(originDepositId);
                DepositEntity destinationDeposit = depositDAO.search(destinationDepositId);

                synchronized (getCacheSyncObject(originDeposit.getId())){
                    synchronized (getCacheSyncObject(destinationDeposit.getId())){

                        depositDAO.refresh(originDeposit);
                        depositDAO.refresh(destinationDeposit);
                        if (originDeposit.getAmount().compareTo(amount) >= 0) {

                            originBalance = originDeposit.getAmount().subtract(amount);
                            originDeposit.setAmount(originBalance);
                            destinationBalance = destinationDeposit.getAmount().add(amount);
                            originDeposit.setAmount(destinationBalance);
                            depositDAO.update(addTransaction(originDeposit , new TransactionEntity(amount.negate() , originBalance , TransactionType.DEPOSIT , getCurrentDate())));
                            depositDAO.update(addTransaction(destinationDeposit , new TransactionEntity(amount , destinationBalance, TransactionType.DEPOSIT , getCurrentDate())));

                        }else {
                            throw new DepositBalanceNotEnoughException("Balance is not enough!");
                        }
                    }
                }

            }catch (Exception e){
//                logHandler.setWarningLog(e.toString());
                throw e;
            }
        }
    }

    @Override
    public void search(String depositId) throws GeneralException {
        depositDAO.search(depositId);

    }

    @Override
    public DepositEntity addTransaction(DepositEntity depositEntity, TransactionEntity transactionEntity) throws GeneralException {
        {

            DepositEntity deposit = depositDAO.search(depositEntity.getDepositNumber());
            deposit.getTransactionEntities().add(transactionEntity);
            return deposit;
        }
    }


    @Override
    public byte[] createReport(String type) throws GeneralException, IOException, DRException {
        return new byte[0];
    }

    @Override
    public ArrayList<DepositEntity> getAllInf() throws GeneralException {
        return depositDAO.getInf();
    }

    @Override
    public void checkIdValidation(DepositEntity depositEntity) throws GeneralException {
        {

            String id = depositEntity.getDepositNumber();
            if (!Objects.equals(id, null)) {
                String regex = "\\d{10}";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(id);
                if (!matcher.matches()) {
                    throw new GeneralException("Invalid id! The id should contain 10 digits");
                }
            }
            else {
//                logHandler.setWarningLog("null field");
                throw new GeneralException("You must enter this item!");
            }

        }
    }
}
