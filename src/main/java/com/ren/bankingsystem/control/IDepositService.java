package com.ren.bankingsystem.control;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.entity.DepositEntity;
import com.ren.bankingsystem.model.entity.TransactionEntity;
import net.sf.dynamicreports.report.exception.DRException;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by My PC on 12/15/2017.
 */
public interface IDepositService extends IService<DepositEntity> {

    DepositEntity insert(DepositEntity deposit, String costumerId) throws GeneralException;

    DepositEntity updateDepositId(String costumerId, String formerDepositId, String newDepositId) throws GeneralException;

    byte[] createDepositReportsForACostumer(String customerId, String type) throws GeneralException, IOException, DRException;

    BigDecimal deposit(String depositId, BigDecimal amount) throws GeneralException;

    BigDecimal withdraw(String depositId, BigDecimal amount) throws GeneralException;

    void transfer(String originDepositId, String destinationDepositId, BigDecimal amount) throws GeneralException;

    void search(String depositId) throws GeneralException;

    DepositEntity addTransaction(DepositEntity depositEntity, TransactionEntity transactionEntity) throws GeneralException;

}