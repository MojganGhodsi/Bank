//package com.ren.bankingsystem.config;
//
//import com.ren.bankingsystem.log.ILogHandler;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.beans.factory.annotation.Autowired;
//
///**
// * Created by My PC on 12/14/2017.
// */
//
////@Component
////@Aspect
//public class LoggingAspect {
//
//    private ILogHandler logHandler;
//
//    @Autowired
//    public LoggingAspect(ILogHandler logHandler) {
//        this.logHandler = logHandler;
//    }
////    @After("within(com.ren.bankingsystem.facade..*)")
////    public void logAfter(JoinPoint joinPoint) {
////        System.out.println("logAfter() is running!");
////        System.out.println("name : " + joinPoint.getSignature().getName());
////        System.out.println("******");
////
////    }
//    @Before("within(com.ren.bankingsystem.facade..*) || within(com.ren.bankingsystem.model..*) || " +
//            "within(com.ren.bankingsystem.control..*) || within(com.ren.bankingsystem.view..*)")
//    public void logBeforeMethod(JoinPoint joinPoint){
//        logHandler.setInfoLog("Method " + joinPoint.getSignature().getName() + "Started");
//    }
//
//    @After("within(com.ren.bankingsystem.facade..*) || within(com.ren.bankingsystem.model..*) || " +
//            "within(com.ren.bankingsystem.control..*) || within(com.ren.bankingsystem.view..*)")
//    public void logAfterMethod(JoinPoint joinPoint){
//        logHandler.setInfoLog("Method " + joinPoint.getSignature().getName() + "Finished");
//    }
//}
