package com.ren.bankingsystem.view.UIBean.insert;


import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.view.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Scope("view")
public class Transfer {

    private String originDepositId ;
    private String destinationDepositId;
    private String amount;

    private IDepositProxy depositProxy;
//
//    @Autowired
//    public Transfer(IDepositProxy depositProxy) {
//        this.depositProxy = depositProxy;
//    }
//
//    public Transfer() {
//    }

    @Autowired
    private IFacade facade;
    public String getOriginDepositId() {
        return originDepositId;
    }

    public void setOriginDepositId(String originDepositId) {
        this.originDepositId = originDepositId;
    }

    public String getDestinationDepositId() {
        return destinationDepositId;
    }

    public void setDestinationDepositId(String destinationDepositId) {
        this.destinationDepositId = destinationDepositId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public IDepositProxy getDepositProxy() {
        return depositProxy;
    }

    public void setDepositProxy(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public void transfer() throws GeneralException {

        facade.transfer(originDepositId,destinationDepositId,new BigDecimal(amount));
//        try {
//            depositProxy.transfer(new ThreeParameterWrapper<>(new DepositDTO(originDepositId), new DepositDTO(destinationDepositId) , new BigDecimal(amount)));
//        }catch (GeneralException e){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }
}
