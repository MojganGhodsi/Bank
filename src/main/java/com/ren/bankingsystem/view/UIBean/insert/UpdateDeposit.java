package com.ren.bankingsystem.view.UIBean.insert;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class UpdateDeposit {

    private String customerId;
    private String depositId;
    private String newDepositId;

    @Autowired
    private IFacade facade;
//    private IDepositProxy depositProxy;
//    private ILogHandler logHandler;
//
//    @Autowired
//    public UpdateDeposit( IDepositProxy depositProxy) {//ILogHandler logHandler
////        this.logHandler = logHandler;
//        this.depositProxy = depositProxy;
//    }

    public UpdateDeposit() {
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getNewDepositId() {
        return newDepositId;
    }

    public void setNewDepositId(String newDepositId) {
        this.newDepositId = newDepositId;
    }

    public void update() throws GeneralException {
//
//        try {
//            depositProxy.update(new ThreeParameterWrapper<>(customerId, depositId, newDepositId));
//        } catch (GeneralException e) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
        facade.updateCustomerId(depositId,newDepositId);
    }
}
