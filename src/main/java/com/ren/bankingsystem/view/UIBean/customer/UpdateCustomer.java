package com.ren.bankingsystem.view.UIBean.customer;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by My PC on 12/15/2017.
 */
@Component
@Scope("view")
public class UpdateCustomer implements Serializable {
    private String customerNumber;
    private String newCustomerNumber;

//    private ICustomerProxy customerProxy;
//
//    @Autowired
//    public UpdateCustomer(ICustomerProxy customerProxy) {
//        this.customerProxy = customerProxy;
//    }


    @Autowired
    private IFacade facade;
    public UpdateCustomer() {
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getNewCustomerNumber() {
        return newCustomerNumber;
    }

    public void setNewCustomerNumber(String newCustomerNumber) {
        this.newCustomerNumber = newCustomerNumber;
    }

    public void update() throws GeneralException {
        facade.updateCustomerId(customerNumber,newCustomerNumber);
//        try {
//            customerProxy.update(new TwoParameterWrapper<>(customerNumber, newCustomerNumber));
//        }catch (GeneralException e){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }
}
