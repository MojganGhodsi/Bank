package com.ren.bankingsystem.view.UIBean.insert;

import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.view.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by My PC on 12/15/2017.
 */
@Component
@Scope("view")
public class InsertDeposit {
    private DepositDTO depositDTO = new DepositDTO();
    private String customerNumber;

    private IDepositProxy depositProxy;
//    private ILogHandler logHandler;

    @Autowired
    private IFacade facade;
//    @Autowired
//    public InsertDeposit(IDepositProxy depositProxy ) {//, ILogHandler logHandler
//        this.depositProxy = depositProxy;
//        this.logHandler = logHandler;
//    }

    public InsertDeposit() {
    }

    public DepositDTO getDepositDTO() {
        return depositDTO;
    }

    public void setDepositDTO(DepositDTO depositDTO) {
        this.depositDTO = depositDTO;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void insert() throws GeneralException {
        facade.insertDeposit(depositDTO,customerNumber);
//
//        try {
//            depositProxy.insert(new TwoParameterWrapper<>(depositDTO, customerNumber));
//        }catch (GeneralException e){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }
}
