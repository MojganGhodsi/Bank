package com.ren.bankingsystem.view.UIBean.customer;

/**
 * Created by My PC on 12/13/2017.
 */

import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.view.proxy.ICustomerProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Scope("view")
public class InsertCustomer implements Serializable {


    private CustomerDTO customerDTO = new CustomerDTO();

    private ICustomerProxy customerProxy;

//    @Autowired
//    public InsertCustomer(ICustomerProxy customerProxy) {
//        this.customerProxy = customerProxy;
//    }


    @Autowired
    private IFacade facade;
    public InsertCustomer(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public InsertCustomer() {
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public void insert() throws GeneralException {

        facade.insertCustomer(customerDTO);

//
//        try{
//            customerProxy.insert(customerDTO);
//        }catch (GeneralException e){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), ""));
//        }
    }
}