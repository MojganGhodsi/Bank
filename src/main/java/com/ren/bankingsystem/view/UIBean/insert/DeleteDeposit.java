package com.ren.bankingsystem.view.UIBean.insert;


import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class DeleteDeposit {

    private DepositDTO depositDTO = new DepositDTO();

//    private IDepositProxy depositProxy;
//    @Autowired
//    public DeleteDeposit(IDepositProxy depositProxy) {
//        this.depositProxy = depositProxy;
//    }

    @Autowired
    private IFacade facade;
    public DeleteDeposit() {
    }

    public DepositDTO getDepositDTO() {
        return depositDTO;
    }

    public void setDepositDTO(DepositDTO depositDTO) {
        this.depositDTO = depositDTO;
    }

    public void delete() throws GeneralException {
        facade.deleteDeposit(depositDTO);
//
//        try {
//            depositProxy.delete(depositDTO);
//        } catch (GeneralException e) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }
}
