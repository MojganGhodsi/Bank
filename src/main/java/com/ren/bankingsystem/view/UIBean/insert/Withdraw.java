package com.ren.bankingsystem.view.UIBean.insert;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Scope("view")
public class Withdraw {

    private String depositId;
    private String amount;

//    private IDepositProxy depositProxy;
//
//    @Autowired
//    public Withdraw(IDepositProxy depositProxy) {
//        this.depositProxy = depositProxy;
//    }

    @Autowired
    private IFacade facade;
    public Withdraw() {
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void withdraw() throws GeneralException {
        facade.withdraw(depositId,new BigDecimal(amount));
//        try{
//            depositProxy.withdraw(new TwoParameterWrapper<>(new DepositDTO(depositId), new BigDecimal(amount)));
//
//        }catch (GeneralException e) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }
}
