package com.ren.bankingsystem.view.UIBean.customer;

import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by My PC on 12/15/2017.
 */
@Component
@Scope("view")
public class DeleteCustomer implements Serializable {


    private String customerNumber;
    private CustomerDTO c= new CustomerDTO(customerNumber);

//    private ICustomerProxy customerProxy;

//    @Autowired
//    public DeleteCustomer(ICustomerProxy customerProxy) {
//        this.customerProxy = customerProxy;
//    }

    @Autowired
    private IFacade facade;

    public DeleteCustomer() {
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void delete() throws GeneralException {
        facade.deleteCustomer(c);
//        try {
//            customerProxy.delete(new CustomerDTO(customerNumber));
//        } catch (GeneralException e) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
//                    e.getMessage(), ""));
//        }
    }

}

