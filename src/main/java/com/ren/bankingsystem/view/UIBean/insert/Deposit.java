package com.ren.bankingsystem.view.UIBean.insert;

import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.view.proxy.IDepositProxy;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.math.BigDecimal;

@Component
@Scope("view")
public class Deposit {

    private String depositId;
    private String amount;

    private IDepositProxy depositProxy;
    @Autowired
    public Deposit(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }


    public Deposit(String depositId, String amount) {
        this.depositId = depositId;
        this.amount = amount;
    }

    public Deposit() {
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void deposit(){

        try {

            depositProxy.deposit(new TwoParameterWrapper<>(new DepositDTO(depositId), new BigDecimal(amount)));
        }catch (GeneralException e){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    e.getMessage(), ""));
        }
    }
}
