//package com.ren.bankingsystem.view.proxy;
//
//
//import com.ren.bankingsystem.exception.GeneralException;
//import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
//
//public interface IReportProxy {
//
//    byte[] depositReport(String type) throws GeneralException;
//
//    byte[] customerReport(String type) throws GeneralException;
//
//    byte[] aCustomerDepositReport(TwoParameterWrapper<String, String> twoParameterWrapper) throws GeneralException;
//}
