//package com.ren.bankingsystem.view.proxy.impl;
//
//
//import com.ren.bankingsystem.view.proxy.IBackupProxy;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.*;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.context.FacesContext;
//
//@Component
//public class BackupProxy implements IBackupProxy {
//
//    private String rootUri = RestURIConstants.MAIN_URI+RestURIConstants.BACKUP;
//
//    private <T> T callRest(String address, Object sendObject , ParameterizedTypeReference<ResponseDTO<T>> t) throws GeneralException {
//        RestTemplate restTemplate = new RestTemplate();
//
//        HttpHeaders requestHeaders = new HttpHeaders();
//
//        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
//        requestHeaders.add("Cookie", headerValue);
//
//        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
//        HttpEntity requestEntity = new HttpEntity(sendObject, requestHeaders);
//        ResponseEntity<ResponseDTO<T>> response = restTemplate.exchange(
//                address,
//                HttpMethod.POST,
//                requestEntity,
//                t);
//        ResponseDTO<T> responseDto = response.getBody();
//        if(responseDto.getResponseStatus().equals(ResponseStatus.OK)){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, responseDto.getMsg() , ""));
//        }else {
//            throw new GeneralException(responseDto.getMsg());
//        }
//        return responseDto.getResponse();
//    }
//
//    @Override
//    public byte[] export()throws GeneralException{
//        return callRest(rootUri+RestURIConstants.EXPORT_BACKUP, null, new ParameterizedTypeReference<ResponseDTO<byte[]>>() {});
//    }
//
//    @Override
//    public byte[] importBackup(byte[] contents) throws GeneralException {
//        return callRest(rootUri+RestURIConstants.IMPORT_BACKUP, contents, new ParameterizedTypeReference<ResponseDTO<byte[]>>() {});
//    }
//}
