//package com.ren.bankingsystem.view.proxy.impl;
//
//
//import com.ren.bankingsystem.dto.ResponseDTO;
//import com.ren.bankingsystem.enums.ResponseStatus;
//import com.ren.bankingsystem.exception.GeneralException;
//import com.ren.bankingsystem.view.RestURIConstants;
//import com.ren.bankingsystem.view.proxy.IReportProxy;
//import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.*;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.context.FacesContext;
//
//@Component
//public class ReportProxy implements IReportProxy {
//
//    private String rootUri = RestURIConstants.MAIN_URI+RestURIConstants.REPORT;
//
//    private  <T> T callRest(String address, Object sendObject , ParameterizedTypeReference<ResponseDTO<T>> t) throws GeneralException {
//
//        RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders requestHeaders = new HttpHeaders();
//
//        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
//        requestHeaders.add("Cookie", headerValue);
//
//        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
//        HttpEntity requestEntity = new HttpEntity(sendObject, requestHeaders);
//        ResponseEntity<ResponseDTO<T>> response = restTemplate.exchange(
//                address,
//                HttpMethod.POST,
//                requestEntity,
//                t);
//        ResponseDTO<T> responseDto = response.getBody();
//        if(responseDto.getResponseStatus().equals(ResponseStatus.OK)){
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, responseDto.getMsg() , ""));
//        }else {
//            throw new GeneralException(responseDto.getMsg());
//        }
//        return responseDto.getResponse();
//    }
//
//    @Override
//    public byte[] depositReport(String type) throws GeneralException {
//
//        return callRest(rootUri+RestURIConstants.DEPOSIT_REPORT, type, new ParameterizedTypeReference<ResponseDTO<byte[]>>() {});
//    }
//
//    @Override
//    public byte[] customerReport(String type) throws GeneralException {
//
//        return callRest(rootUri+RestURIConstants.CUSTOMER_REPORT, type, new ParameterizedTypeReference<ResponseDTO<byte[]>>() {});
//    }
//
//    @Override
//    public byte[] aCustomerDepositReport(TwoParameterWrapper<String, String> twoParameterWrapper) throws GeneralException {
//
//        return callRest(rootUri+RestURIConstants.A_CUSTOMER_REPORT, twoParameterWrapper, new ParameterizedTypeReference<ResponseDTO<byte[]>>() {});
//    }
//
//
//}
