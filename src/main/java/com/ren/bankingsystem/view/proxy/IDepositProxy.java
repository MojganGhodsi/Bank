package com.ren.bankingsystem.view.proxy;


import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.wrapper.ThreeParameterWrapper;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;

import java.math.BigDecimal;

public interface IDepositProxy {

    TwoParameterWrapper<DepositDTO,String> insert(TwoParameterWrapper<DepositDTO, String> twoParameterWrapper) throws GeneralException;

    ThreeParameterWrapper<String,String,String> update(ThreeParameterWrapper<String, String, String> threeParameterWrapper) throws GeneralException;

    DepositDTO delete(DepositDTO depositDTO) throws GeneralException;

    TwoParameterWrapper<DepositDTO,BigDecimal> deposit(TwoParameterWrapper<DepositDTO, BigDecimal> twoParameterWrapper) throws GeneralException;

    TwoParameterWrapper<DepositDTO,BigDecimal> withdraw(TwoParameterWrapper<DepositDTO, BigDecimal> twoParameterWrapper) throws GeneralException;

    ThreeParameterWrapper<DepositDTO,DepositDTO,BigDecimal> transfer(ThreeParameterWrapper<DepositDTO, DepositDTO, BigDecimal> threeParameterWrapper) throws GeneralException;

//   DepositDTO payADepositProfit(DepositDTO depositDTO);
}

