package com.ren.bankingsystem.view.proxy.impl;

import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.dto.ResponseDTO;
import com.ren.bankingsystem.enums.ResponseStatus;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.view.RestURIConstants;
import com.ren.bankingsystem.view.proxy.IDepositProxy;
import com.ren.bankingsystem.wrapper.ThreeParameterWrapper;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.math.BigDecimal;

@Component
public class DepositProxy implements IDepositProxy {

    //root uri of deposit controller
    private String rootUri = RestURIConstants.MAIN_URI+RestURIConstants.DEPOSIT;

    private  <T> T callRest(String address, Object sendObject , ParameterizedTypeReference<ResponseDTO<T>> t) throws GeneralException {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestEntity = new HttpEntity(sendObject, requestHeaders);
        ResponseEntity<ResponseDTO<T>> response = restTemplate.exchange(
                address,
                HttpMethod.POST,
                requestEntity,
                t);
        ResponseDTO<T> responseDto = response.getBody();
        if(responseDto.getResponseStatus().equals(ResponseStatus.OK)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, responseDto.getMsg() , ""));
        }else {
            throw new GeneralException(responseDto.getMsg());
        }
        return responseDto.getResponse();
    }

    @Override
    public TwoParameterWrapper<DepositDTO, String> insert(TwoParameterWrapper<DepositDTO, String> twoParameterWrapper) throws GeneralException {

        return callRest(rootUri+RestURIConstants.INSERT_DEPOSIT ,twoParameterWrapper, new ParameterizedTypeReference<ResponseDTO<TwoParameterWrapper<DepositDTO,String>>>(){});
    }

    @Override
    public ThreeParameterWrapper<String, String, String> update(ThreeParameterWrapper<String, String, String> threeParameterWrapper) throws GeneralException {

        return callRest(rootUri+RestURIConstants.UPDATE_DEPOSIT ,threeParameterWrapper, new ParameterizedTypeReference<ResponseDTO<ThreeParameterWrapper<String,String,String>>>(){});
    }

    @Override
    public DepositDTO delete(DepositDTO depositDTO) throws GeneralException {

        return callRest(rootUri+RestURIConstants.DELETE_DEPOSIT ,depositDTO, new ParameterizedTypeReference<ResponseDTO<DepositDTO>>(){});
    }

    @Override
    public TwoParameterWrapper<DepositDTO, BigDecimal> deposit(TwoParameterWrapper<DepositDTO, BigDecimal> twoParameterWrapper) throws GeneralException {

        return callRest(rootUri+RestURIConstants.DEPOSIT_DEPOSIT ,twoParameterWrapper, new ParameterizedTypeReference<ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>>>(){});
    }

    @Override
    public TwoParameterWrapper<DepositDTO, BigDecimal> withdraw(TwoParameterWrapper<DepositDTO, BigDecimal> twoParameterWrapper) throws GeneralException {
        return callRest(rootUri+RestURIConstants.WITHDRAW_DEPOSIT ,twoParameterWrapper, new ParameterizedTypeReference<ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>>>(){});
    }

    @Override
    public ThreeParameterWrapper<DepositDTO, DepositDTO, BigDecimal> transfer(ThreeParameterWrapper<DepositDTO, DepositDTO, BigDecimal> threeParameterWrapper) throws GeneralException {
        return callRest(rootUri+RestURIConstants.TRANSFER_DEPOSIT ,threeParameterWrapper, new ParameterizedTypeReference<ResponseDTO<ThreeParameterWrapper<DepositDTO,DepositDTO,BigDecimal>>>(){});
    }

}
