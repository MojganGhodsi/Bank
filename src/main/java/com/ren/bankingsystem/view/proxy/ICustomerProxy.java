package com.ren.bankingsystem.view.proxy;


import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;

public interface ICustomerProxy {

    CustomerDTO delete(CustomerDTO customerDTO) throws GeneralException;

    CustomerDTO insert(CustomerDTO customerDTO) throws GeneralException;

    TwoParameterWrapper<String,String> update(TwoParameterWrapper<String, String> twoParameterWrapper) throws GeneralException;
}
