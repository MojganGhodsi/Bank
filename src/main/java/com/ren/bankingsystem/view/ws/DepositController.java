package com.ren.bankingsystem.view.ws;

import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.dto.ResponseDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.view.RestURIConstants;
import com.ren.bankingsystem.wrapper.ThreeParameterWrapper;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.ren.bankingsystem.enums.ResponseStatus;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = RestURIConstants.DEPOSIT)
public class DepositController {

    private IFacade facade;

    @Autowired
    public DepositController(IFacade facade) {

        this.facade = facade;
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.INSERT_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<TwoParameterWrapper<DepositDTO,String>>> insert(@RequestBody TwoParameterWrapper<DepositDTO,String> twoParameterWrapper){

        ResponseDTO<TwoParameterWrapper<DepositDTO,String>> responseDTO = new ResponseDTO<>();
        try {
            facade.insertDeposit(twoParameterWrapper.getT() , twoParameterWrapper.getV());
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Deposit Inserted successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.UPDATE_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<ThreeParameterWrapper<String,String,String>>> update (@RequestBody ThreeParameterWrapper<String,String,String> threeParameterWrapper){

        ResponseDTO<ThreeParameterWrapper<String,String,String>> responseDTO = new ResponseDTO<>();
        try {
            facade.updateDeposit(threeParameterWrapper.getT() , threeParameterWrapper.getV() , threeParameterWrapper.getE());
            responseDTO.setResponse(threeParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Deposit updated successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(threeParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.DELETE_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<DepositDTO>> delete(@RequestBody DepositDTO depositDTO){

        ResponseDTO<DepositDTO> responseDTO = new ResponseDTO<>();
        try {
            facade.deleteDeposit(depositDTO);
            responseDTO.setResponse(depositDTO);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Deposit deleted successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(depositDTO);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.DEPOSIT_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>>> deposit (@RequestBody TwoParameterWrapper<DepositDTO,BigDecimal> twoParameterWrapper){

        ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>> responseDTO = new ResponseDTO<>();
        try {
            facade.deposit(twoParameterWrapper.getT().getDepositNumber(),twoParameterWrapper.getV());
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Done!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.WITHDRAW_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>>> withdraw (@RequestBody TwoParameterWrapper<DepositDTO,BigDecimal> twoParameterWrapper){

        ResponseDTO<TwoParameterWrapper<DepositDTO,BigDecimal>> responseDTO = new ResponseDTO<>();
        try {
            facade.withdraw(twoParameterWrapper.getT().getDepositNumber(),twoParameterWrapper.getV());
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Done!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.TRANSFER_DEPOSIT)
    public @ResponseBody ResponseEntity<ResponseDTO<ThreeParameterWrapper<DepositDTO,DepositDTO,BigDecimal>>> transfer (@RequestBody ThreeParameterWrapper<DepositDTO,DepositDTO,BigDecimal> threeParameterWrapper){

        ResponseDTO<ThreeParameterWrapper<DepositDTO,DepositDTO,BigDecimal>> responseDTO = new ResponseDTO<>();
        try {
            facade.transfer(threeParameterWrapper.getT().getDepositNumber(),threeParameterWrapper.getV().getDepositNumber() , threeParameterWrapper.getE());
            responseDTO.setResponse(threeParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Done!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(threeParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }


}
