package com.ren.bankingsystem.view.ws;

import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.dto.ResponseDTO;
import com.ren.bankingsystem.enums.ResponseStatus;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.view.RestURIConstants;
import com.ren.bankingsystem.wrapper.TwoParameterWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = RestURIConstants.CUSTOMER)
public class CustomerController {

    private IFacade facade;

    @Autowired
    public CustomerController(IFacade facade) {
        this.facade = facade;
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.INSERT_CUSTOMER)
    public @ResponseBody
    ResponseEntity<ResponseDTO<CustomerDTO>> insert(@RequestBody CustomerDTO customerDTO){

        ResponseDTO<CustomerDTO> responseDTO = new ResponseDTO<>();
        try {
            facade.insertCustomer(customerDTO);
            responseDTO.setResponse(customerDTO);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("customer Inserted Successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(customerDTO);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.UPDATE_CUSTOMER)
    public @ResponseBody ResponseEntity<ResponseDTO<TwoParameterWrapper<String,String>>> update(@RequestBody TwoParameterWrapper<String,String> twoParameterWrapper){

        ResponseDTO<TwoParameterWrapper<String,String>> responseDTO = new ResponseDTO<>();
        try {
            facade.updateCustomerId(twoParameterWrapper.getT() , twoParameterWrapper.getV());
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Customer updated successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(twoParameterWrapper);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.DELETE_CUSTOMER)
    public @ResponseBody ResponseEntity<ResponseDTO<CustomerDTO>> delete(@RequestBody CustomerDTO customerDTO){

        ResponseDTO<CustomerDTO> responseDTO = new ResponseDTO<>();
        try {
            facade.deleteCustomer(customerDTO);
            responseDTO.setResponse(customerDTO);
            responseDTO.setResponseStatus(ResponseStatus.OK);
            responseDTO.setMsg("Customer deleted successfully!");
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (GeneralException e) {
            responseDTO.setResponse(customerDTO);
            responseDTO.setResponseStatus(ResponseStatus.ERROR);
            responseDTO.setMsg(e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        }
    }

}
