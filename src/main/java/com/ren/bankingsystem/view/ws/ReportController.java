//package com.ren.bankingsystem.view.ws;
//
//import net.sf.dynamicreports.report.exception.DRException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import com.ren.bankingsystem.enums.ResponseStatus;
//import java.io.IOException;
//
//@RestController
//@RequestMapping(value = RestURIConstants.REPORT)
//public class ReportController {
//
//    private IFacade facade;
//    private ILogHandler logHandler;
//
//    @Autowired
//    public ReportController(IFacade facade, ILogHandler logHandler) {
//        this.facade = facade;
//        this.logHandler = logHandler;
//    }
//
//    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.CUSTOMER_REPORT)
//    public @ResponseBody ResponseEntity<ResponseDTO<byte[]>> createCustomersReport (@RequestBody String type){
//
//        ResponseDTO<byte[]> responseDTO = new ResponseDTO<>();
//        try {
//            byte[] response = facade.createCustomersReport(type);
//            responseDTO.setResponse(response);
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.OK);
//            responseDTO.setMsg("Report created Successfully! Press button again to download it!");
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//
//        } catch (DRException | GeneralException | IOException e) {
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.ERROR);
//            responseDTO.setMsg(e.getMessage());
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//        }
//    }
//
//    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.DEPOSIT_REPORT)
//    public @ResponseBody ResponseEntity<ResponseDTO<byte[]>> createDepositsReport(@RequestBody String type){
//
//        ResponseDTO<byte[]> responseDTO = new ResponseDTO<>();
//        try {
//            byte[] response = facade.createDepositsReport(type);
//            responseDTO.setResponse(response);
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.OK);
//            responseDTO.setMsg("Report created Successfully! Press button again to download it!");
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//
//        } catch (DRException | GeneralException | IOException e) {
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.ERROR);
//            responseDTO.setMsg(e.getMessage());
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//        }
//    }
//
//    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.A_CUSTOMER_REPORT)
//    public @ResponseBody
//    ResponseEntity<ResponseDTO<byte[]>> createACustomerDepositsReport (@RequestBody TwoParameterWrapper<String,String> twoParameterWrapper){
//
//        ResponseDTO<byte[]> responseDTO = new ResponseDTO<>();
//        try {
//            byte[] response = facade.createDepositsReportForACustomer(twoParameterWrapper.getT(),twoParameterWrapper.getV());
//            responseDTO.setResponse(response);
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.OK);
//            responseDTO.setMsg("Report created Successfully! Press button again to download it!");
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//
//        } catch (DRException | IOException | GeneralException e) {
//            responseDTO.setResponseStatus(com.company.costumerService.enums.ResponseStatus.ERROR);
//            responseDTO.setMsg(e.getMessage());
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//        }
//    }
//}
