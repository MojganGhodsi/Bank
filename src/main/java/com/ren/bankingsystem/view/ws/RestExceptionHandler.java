package com.ren.bankingsystem.view.ws;

import com.ren.bankingsystem.dto.ResponseDTO;
import com.ren.bankingsystem.enums.ResponseStatus;
import com.ren.bankingsystem.exception.GeneralException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

    @ExceptionHandler(value = {Throwable.class})
    ResponseEntity<ResponseDTO> handle (Throwable t){

        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseStatus(ResponseStatus.ERROR);
        if (t instanceof AccessDeniedException){
            responseDTO.setMsg("Your not allowed!");
        }else if (t instanceof GeneralException){
            responseDTO.setMsg(t.getMessage());
        }else {
            responseDTO.setMsg("Problems Found!");
        }
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }


}
