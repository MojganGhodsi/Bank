//package com.ren.bankingsystem.view.ws;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import com.ren.bankingsystem.enums.ResponseStatus;

//@RestController
//@RequestMapping(value = RestURIConstants.BACKUP)
//public class BackupController {
//
//    private IFacade facade;
//    private ILogHandler logHandler;
//
//    @Autowired
//    public BackupController(IFacade facade , ILogHandler logHandler) {
//        this.facade = facade;
//        this.logHandler = logHandler;
//    }
//
//    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.EXPORT_BACKUP)
//    @ResponseBody ResponseEntity<ResponseDTO<byte[]>> exportBackup(){
//
//        ResponseDTO<byte[]> responseDTO = new ResponseDTO<>();
//        try {
//            byte[] backup = facade.generateBackup();
//            responseDTO.setResponse(backup);
//            responseDTO.setResponseStatus(ResponseStatus.OK);
//            responseDTO.setMsg("BackUp exported successfully!");
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//
//        } catch (GeneralException e) {
//            logHandler.setWarningLog(e.getMessage());
//            responseDTO.setResponse(null);
//            responseDTO.setResponseStatus(ResponseStatus.OK);
//            responseDTO.setMsg(e.getMessage());
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//        }
//    }
//
//    @RequestMapping(method = RequestMethod.POST , value = RestURIConstants.IMPORT_BACKUP)
//    @ResponseBody ResponseEntity<ResponseDTO<byte[]>> importBackup(@RequestBody byte[] content){
//
//        ResponseDTO<byte[]> responseDTO = new ResponseDTO<>();
//        try {
//            byte[] backup = facade.returnBackup(content);
//            responseDTO.setResponse(backup);
//            responseDTO.setResponseStatus(ResponseStatus.OK);
//            responseDTO.setMsg("BackUp imported successfully!");
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//
//        } catch (GeneralException e) {
//            logHandler.setWarningLog(e.getMessage());
//            responseDTO.setResponse(null);
//            responseDTO.setResponseStatus(ResponseStatus.OK);
//            responseDTO.setMsg(e.getMessage());
//            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
//        }
//    }
//}
