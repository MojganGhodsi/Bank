//package com.ren.bankingsystem.log.impl;
//
//import com.ren.bankingsystem.exception.GeneralException;
//import com.ren.bankingsystem.log.ILogHandler;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import java.io.IOException;
//import java.util.logging.FileHandler;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//@Component
//public class LogHandler implements ILogHandler {
//
//    private static final Logger logger = Logger.getLogger("LogFile");
//    private FileHandler file;
//
//    @PostConstruct
//    public void setLogHandler() throws GeneralException {
//
//        String path = "F:\\log.txt";
//        try {
//
//            file = new FileHandler(path , true);
//            logger.addHandler(file);
//            logger.setLevel(Level.INFO);
//
//        } catch (IOException e) {
//            throw new GeneralException("Problems found!" , e);
//        }
//    }
//
//    @Override
//    public void setInfoLog(String msg){
//
//        logger.info(msg);
//    }
//
//    @Override
//    public void setWarningLog(String msg){
//
//        logger.warning(msg);
//    }
//
//    @PreDestroy
//    public void closeFile(){
//
//        file.close();
//    }
//
//}
