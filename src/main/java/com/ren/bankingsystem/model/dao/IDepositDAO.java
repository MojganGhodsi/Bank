package com.ren.bankingsystem.model.dao;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.entity.DepositEntity;

import java.util.ArrayList;

/**
 * Created by My PC on 12/10/2017.
 */
public interface IDepositDAO extends IBaseDAO<DepositEntity>{

    ArrayList<DepositEntity> getCostumerDepositsInf(String contactId) throws GeneralException;
}
