package com.ren.bankingsystem.model.dao.impl;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.dao.IDepositDAO;
import com.ren.bankingsystem.model.entity.DepositEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Created by My PC on 12/10/2017.
 */
@Repository
public class DepossitDAO extends AbstractDAO implements IDepositDAO{


    @Override
    public ArrayList<DepositEntity> getCostumerDepositsInf(String customerNumber) throws GeneralException {

        String jpql = "select deposit from DepositEntity deposit where deposit.customer.customerNumber =:costumerNumber" ;
        Query query = entityManager.createQuery(jpql);
        query.setParameter("costumerNumber" , customerNumber);

        try{
            return (ArrayList<DepositEntity>) query.getResultList();

        } catch (Exception e){
            throw new GeneralException("No item matches!" , e);
        }
    }

    @Override
    public void delete(DepositEntity depositEntity) throws GeneralException {

        try{
            entityManager.remove(depositEntity);
        }catch (PersistenceException e){
            throw new GeneralException("No item matches" , e);
        }
    }

    @Override
    public DepositEntity update(DepositEntity depositEntity) throws GeneralException {

        try{
            entityManager.merge(depositEntity);
            return depositEntity;

        }catch (PersistenceException e){
            throw new GeneralException("The deposit id that you enter is been used!" , e);
        }
        catch (Exception e){
            throw new GeneralException("Problems found!" , e);
        }
    }

    @Override
    public ArrayList<DepositEntity> getInf() throws GeneralException {

        String jpql = "select deposit from DepositEntity deposit" ;
        Query query = entityManager.createQuery(jpql);

        try{
            return (ArrayList<DepositEntity>) query.getResultList();

        } catch (Exception e){
            throw new GeneralException("There isn't any Deposit yet!" , e);
        }
    }

    @Override
    public void refresh(DepositEntity depositEntity) throws GeneralException {

    }

    @Override
    public boolean checkIdValidation(String id) {
        return false;
    }

    @Override
    public DepositEntity search(String param) throws GeneralException {
        return null;
    }
}
