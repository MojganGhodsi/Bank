package com.ren.bankingsystem.model.dao.impl;

import com.ren.bankingsystem.exception.CustomerNotFoundException;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.dao.ICustomerDAO;
import com.ren.bankingsystem.model.entity.CustomerEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Created by My PC on 12/10/2017.
 */
@Repository("CostumerDAO")
public class CustomerDAO extends AbstractDAO implements ICustomerDAO {


    @Override
    public CustomerEntity insert(CustomerEntity customer) throws GeneralException {
        try{
            entityManager.persist(customer);

        }catch (PersistenceException e){
            throw new GeneralException("The contacts id that you enter is been used!" , e);
        }
        catch (Exception e){
            throw new GeneralException("Problems found!" , e);
        }
        return customer;
    }

    @Override
    public CustomerEntity update(CustomerEntity customer) throws GeneralException {
        try{
            entityManager.merge(customer);
            return customer;

        }catch (PersistenceException e){
            throw new GeneralException("The contacts id that you enter is been used!" , e);
        }
    }

    @Override
    public void delete(CustomerEntity customer) throws GeneralException {
        entityManager.remove(customer);
    }

    @Override
    public void refresh(CustomerEntity customer) throws GeneralException {
        entityManager.refresh(customer);
    }

    @Override
    public CustomerEntity search(String id) throws GeneralException {

        String jpql = "select costumer from CustomerEntity costumer where costumer.customerNumber =:customerNumber";
        Query query = entityManager.createQuery(jpql);
        query.setParameter("customerNumber" , id);

        try {
            return (CustomerEntity) query.getSingleResult();
        }catch (Exception e){
            throw new CustomerNotFoundException("No item matches!" , e);
        }
    }

    @Override
    public boolean checkIdValidation(String id) {
        String jpql = "select customer from CustomerEntity customer where customer.customerNumber =:customerNumber";
        Query query = entityManager.createQuery(jpql);
        query.setParameter("customerNumber" , id);

        try{
            CustomerEntity customer = (CustomerEntity) query.getSingleResult();
            return false;
        }catch (Exception e){
            return true;
        }
    }

    @Override
    public ArrayList<CustomerEntity> getInf() throws GeneralException {

        String jpql = "select customer from CustomerEntity customer";
        Query query = entityManager.createQuery(jpql);

        try {

            return (ArrayList<CustomerEntity>) query.getResultList();
        }
        catch (Exception e){
            throw new GeneralException("Problems found!" , e);
        }
    }
}
