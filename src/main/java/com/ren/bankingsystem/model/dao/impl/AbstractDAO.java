package com.ren.bankingsystem.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by My PC on 12/10/2017.
 */
public abstract class AbstractDAO {

    @PersistenceContext
    EntityManager entityManager;
}
