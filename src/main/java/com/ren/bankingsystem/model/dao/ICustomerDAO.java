package com.ren.bankingsystem.model.dao;

import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.model.entity.CustomerEntity;

/**
 * Created by My PC on 12/10/2017.
 */
public interface ICustomerDAO extends IBaseDAO<CustomerEntity> {

    CustomerEntity insert(CustomerEntity customerEntity) throws GeneralException;

}