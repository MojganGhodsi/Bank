package com.ren.bankingsystem.model.dao;

import com.ren.bankingsystem.exception.GeneralException;

import java.util.ArrayList;

/**
 * Created by My PC on 12/10/2017.
 */

public interface IBaseDAO<T>{

    void delete(T t) throws GeneralException;

    T update(T t) throws GeneralException;

    ArrayList<T> getInf() throws GeneralException;

    void refresh(T t) throws GeneralException;

    boolean checkIdValidation(String id);

    T search(String param) throws GeneralException;
}
