//package com.ren.bankingsystem.model.entity;
//
//import com.google.gson.annotations.Expose;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//
///**
// * Created by My PC on 12/11/2017.
// */
//@NoArgsConstructor
//@AllArgsConstructor
//@Data
//@Entity
//public class UserEntity {
//
//    @Id
//    @GeneratedValue
//    @Column(name = "Id")
//    private int id;
//
//    @Expose
//    @Column(name = "User Name")
//    private String userName;
//
//    @Expose
//    @Column(name = "Password")
//    private String password;
//
//    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
//    private CustomerEnity customer;
//
//    public UserEntity(String userName, String password) {
//        this.userName= userName;
//        this.password = password;
//    }
//}