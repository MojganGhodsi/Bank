package com.ren.bankingsystem.model.entity;

import com.google.gson.annotations.Expose;
import com.ren.bankingsystem.enums.DepositType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by My PC on 11/4/2017.
 */

@Entity
@Table(name = "Deposit")
public class DepositEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Expose
    @Column(name = "DepositNumber" , unique = true)
    private String depositNumber;

    @Expose
    @Column(name = "Amount")
    private BigDecimal amount;

    @Expose
    @Column(name = "DepositType")
    @Enumerated(EnumType.STRING)
    private DepositType depositType;

    @ManyToOne

    @JoinColumn(name = "CustomerNumber")
    private CustomerEntity customer;

    @Expose
    @OneToMany(cascade = {CascadeType.ALL} , fetch = FetchType.EAGER , orphanRemoval = true)
    @JoinColumn(name = "DEPOSIT_ID")
    private List<TransactionEntity> transactionEntities = new ArrayList<>();

    public DepositEntity() {

    }


    public DepositEntity(String depositNumber, DepositType depositType, BigDecimal amount) {
        this.depositNumber = depositNumber;
        this.depositType = depositType;
        this.amount = amount;
    }
    public DepositEntity(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TransactionEntity> getTransactionEntities() {

        return transactionEntities;
    }

    public void setTransactionEntities(List<TransactionEntity> transactionEntities) {
        this.transactionEntities = transactionEntities;
    }




    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }
}
