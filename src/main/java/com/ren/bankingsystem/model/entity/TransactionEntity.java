package com.ren.bankingsystem.model.entity;

import com.google.gson.annotations.Expose;
import com.ren.bankingsystem.enums.TransactionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by My PC on 12/11/2017.
 */


@Entity
@Table(name = "Transactions")
public class TransactionEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @Expose
    @Column(name = "Amount")
    private BigDecimal amount;

    @Expose
    @Column(name = "Balance")
    private BigDecimal balance;

    @Expose
    @Column(name = "TransactionType")
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Expose
    @Column(name = "TransactionDate")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "DepositNumber")
    private DepositEntity depositEntity;

    /*@ManyToOne
    @JoinColumn(name = "CustomerNumber")
    private CustomerEntity customer;*/

    public TransactionEntity(BigDecimal amount, TransactionType transactionType, Date date) {
        this.amount = amount;
        this.transactionType = transactionType;
        this.date = date;
    }

    public TransactionEntity(BigDecimal amount, BigDecimal balance, TransactionType transactionType, Date date) {
        this.amount = amount;
        this.balance = balance;
        this.transactionType = transactionType;
        this.date = date;
    }

    public TransactionEntity() {
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DepositEntity getDepositEntity() {
        return depositEntity;
    }

    public void setDepositEntity(DepositEntity depositEntity) {
        this.depositEntity = depositEntity;
    }
}