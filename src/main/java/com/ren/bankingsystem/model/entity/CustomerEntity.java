package com.ren.bankingsystem.model.entity;

import com.google.gson.annotations.Expose;
import com.sun.xml.internal.ws.developer.Serialization;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by My PC on 11/4/2017.
 */

@Entity
@Table(name = "customer")
public class CustomerEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private int id;

    @Expose
    @Column(name = "Name")
    private String name;

    @Expose
    @Column(name = "LastName")
    private String familyName;

    @Expose
    @Column(name = "CustomerNumber" , unique = true )
    private String customerNumber;

    @Expose
    @Column(name = "Email")
    private String email;

//    @Expose
//    @OneToOne(cascade = {CascadeType.ALL})
//    private UserEntity user;

    @Expose
    @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "CustomerNumber")
    private List<DepositEntity> deposits = new ArrayList<>();

    public List<DepositEntity> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<DepositEntity> deposits) {
        this.deposits = deposits;
    }

    public CustomerEntity(String costumerNumber) {

        this.customerNumber = customerNumber;
    }

    public CustomerEntity(){

    }


    public CustomerEntity(String name, String familyname, String customerNumber , String email){
        this.name=name;
        this.familyName=familyname;
        this.customerNumber=customerNumber;
        this.email=email;
        }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
