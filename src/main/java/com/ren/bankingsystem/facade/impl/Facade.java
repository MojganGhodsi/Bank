package com.ren.bankingsystem.facade.impl;

import com.ren.bankingsystem.control.IDepositService;
import com.ren.bankingsystem.control.implService.CustomerService;
import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import com.ren.bankingsystem.facade.IFacade;
import com.ren.bankingsystem.model.entity.CustomerEntity;
import com.ren.bankingsystem.model.entity.DepositEntity;
import net.sf.dynamicreports.report.exception.DRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by My PC on 11/3/2017.
 */

@Component("Facade")
@Transactional
public class Facade implements IFacade {


    private CustomerService customerService;
    private IDepositService depositService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    public Facade(CustomerService customerService, IDepositService depositService) {
        this.customerService = customerService;
        this.depositService = depositService;
    }

    @Override
    public void insertCustomer(CustomerDTO customerDTO) throws GeneralException {
        CustomerEntity ce =new CustomerEntity(customerDTO.getName(), customerDTO.getFamilyName(),customerDTO.getCustomerNumber(),customerDTO.getEmail());
        customerService.insert(ce);
    }

    @Override
    public void updateCustomerId(String formerId, String newId) throws GeneralException {
        customerService.updateContactId(formerId , newId);

    }

    @Override
    public void deleteCustomer(CustomerDTO customerDTO) throws GeneralException {

        CustomerEntity ce = new CustomerEntity();
        ce.setCustomerNumber(customerDTO.getCustomerNumber());
        customerService.delete(ce);
    }

    @Override
    public void insertDeposit(DepositDTO depositDTO, String costumerId) throws GeneralException {

        DepositEntity depositEntity = new DepositEntity(depositDTO.getDepositNumber() , depositDTO.getDepositType(), depositDTO.getBalance());
        depositService.insert(depositEntity , costumerId );
    }



    @Override
    public void updateDeposit(String costumerId, String formerDepositId, String newDepositId) throws GeneralException {

        depositService.updateDepositId(costumerId , formerDepositId , newDepositId);

    }

    @Override
    public void deleteDeposit(DepositDTO depositDTO) throws GeneralException {


        DepositEntity depositEntity = new DepositEntity(depositDTO.getDepositNumber() , depositDTO.getDepositType() , depositDTO.getBalance());
        depositService.delete(depositEntity);

    }

    @Override
    public BigDecimal deposit(String depositId, BigDecimal amount) throws GeneralException {

        return depositService.deposit(depositId , amount);

    }

    @Override
    public void transfer(String originDepositId, String destinationDepositId, BigDecimal amount) throws GeneralException {

        depositService.transfer(originDepositId , destinationDepositId , amount);
    }

    @Override
    public BigDecimal withdraw(String depositId, BigDecimal amount) throws GeneralException {

        return depositService.withdraw(depositId , amount);
    }

    @Override
    public byte[] returnBackup(byte[] content) throws GeneralException {

        return customerService.returnBackup(content);
    }

    @Override
    public byte[] generateBackup() throws GeneralException {

        return customerService.generateBackup();
    }

    @Override
    public byte[] createCustomersReport(String type) throws GeneralException, IOException, DRException {

        byte[] report =  customerService.createReport(type);
        return report;
    }

    @Override
    public byte[] createDepositsReport(String type) throws GeneralException, IOException, DRException {

        byte[] report = depositService.createReport(type);
        return report;
    }

    @Override
    public byte[] createDepositsReportForACustomer(String customerId, String type) throws GeneralException, IOException, DRException {

        byte[] report = depositService.createDepositReportsForACostumer(customerId , type);
        return report;
    }


}
