package com.ren.bankingsystem.facade;

import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.exception.GeneralException;
import net.sf.dynamicreports.report.exception.DRException;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by My PC on 11/3/2017.
 */



public interface IFacade {

    void insertCustomer(CustomerDTO CustomerDto) throws GeneralException;


    void updateCustomerId(String formerId, String newId) throws GeneralException;

    void deleteCustomer(CustomerDTO customerDTO) throws GeneralException;

    void insertDeposit(DepositDTO depositDTO, String costumerId) throws GeneralException;

    void updateDeposit(String costumerId, String formerDepositId, String newDepositId) throws GeneralException;


    void deleteDeposit(DepositDTO depositDTO) throws GeneralException;

    BigDecimal deposit(String depositId, BigDecimal amount) throws GeneralException;


    BigDecimal withdraw(String depositId, BigDecimal amount) throws GeneralException;


    void transfer(String originDepositId, String destinationDepositId, BigDecimal amount) throws GeneralException;

    byte[] returnBackup(byte[] content) throws GeneralException;

    byte[] generateBackup() throws GeneralException;

    byte[] createCustomersReport(String type) throws GeneralException, IOException, DRException;

    byte[] createDepositsReport(String type) throws GeneralException, IOException, DRException;

    byte[] createDepositsReportForACustomer(String customerId , String type) throws GeneralException, IOException, DRException;


}
