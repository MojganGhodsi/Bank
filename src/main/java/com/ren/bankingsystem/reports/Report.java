//package com.ren.bankingsystem.reports;
//
//import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
//import net.sf.dynamicreports.report.builder.DynamicReports;
//import com.ren.bankingsystem.properties.PropertyHandeler;
//import com.ren.bankingsystem.log.implService.LogHandeler;
//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.com.ren.bankingsystem.view.util.ArrayList;
//import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
//import net.sf.dynamicreports.report.builder.DynamicReports;
//import net.sf.dynamicreports.report.builder.component.Components;
//import net.sf.dynamicreports.report.builder.expression.PrintInEvenRowExpression;
//import net.sf.dynamicreports.report.builder.style.StyleBuilder;
//import net.sf.dynamicreports.report.constant.HorizontalAlignment;
//import net.sf.dynamicreports.report.constant.VerticalAlignment;
//
//import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
//
///**
// * Created by My PC on 11/4/2017.
// */
//public abstract class Report{
//
//    private JasperReportBuilder report = null;
//    private LogHandeler logHandeler = LogHandeler.getInstance();
//
//    private PropertyHandeler property;
//    public Report() {
//        report = DynamicReports.report();
//    }
//
//    public void generate(ArrayList<ContactEntity> contacts) throws CostumeException {
//
//        new Thread(() -> {
//            try {
//                setReportsDesign();
//                setColumns();
//                setData(contacts);
//                save();
//            } catch (CostumeException e) {
//                logHandeler.getInstance().setWarningLog(e.toS);
//            }
//        }).start();
//    }
//
//
//    private void setReportsDesign() throws CostumeException {
//
//        StyleBuilder boldStyle = stl.style().bold();
//
//        StyleBuilder boldCenteredStyle = stl.style(boldStyle)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        StyleBuilder titleStyle = stl.style(boldCenteredStyle)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setFontSize(15);
//
//        BufferedImage img ;
//        try {
//
//            String path = PropertyHandler.getInstance().getProperties("ReportImageDirectory");
//            ClassLoader classLoader = getClass().getClassLoader();
//            img = ImageIO.read(new File(classLoader.getResource(path).getFile()));
//
//        } catch (IOException e) {
//            LogHandler.getInstance().setWarningLog(e.toString());
//            //throw new CostumeException("Problems found!" , e);
//        }
//
//        report.highlightDetailEvenRows()
//                .title(cmp.horizontalList().add(
//                        cmp.image(img).setFixedDimension(80, 80) ,
//                        cmp.text("DynamicReports").setStyle(titleStyle).setHorizontalAlignment(HorizontalAlignment.LEFT))
//                        .newRow()
//                        .add(cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10)))
//                .pageFooter(Components.pageXofY().setStyle(boldCenteredStyle));
//        //JRProperties.setProperty(QueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX+"plsql" ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
//    }
//
//    public abstract void setColumns();
//
//    public abstract void setData(ArrayList<ContactEntity> contacts);
//
//    public abstract void save() ///throws CostumeException;
//
//    JasperReportBuilder getReport() {
//        return report;
//    }
//
//
//}
